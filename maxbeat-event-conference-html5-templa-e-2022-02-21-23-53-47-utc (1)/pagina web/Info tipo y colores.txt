Sarc página web

1. Tipografía - Open sans y Philosopher (google fonts)

2. Colores:

	Primarios
	a. Turquesa:#00A99D
	B. Negro: #1A1A1A
	C. Blanco: #FFFFFF

	Secundarios
	D. Verde pino: #037C70
	E. Azul: #7FD3CD
	F. Gris oscuro: #333333
	G. Gris: #CCCCCC
	H. Blanco humo: #F2F2F2